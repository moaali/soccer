import React, { Suspense, lazy } from 'react';
import { hot } from 'react-hot-loader';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { always } from 'ramda';
import 'react-toastify/dist/ReactToastify.css';

import { configureStore } from 'state';

import 'antd/dist/antd.min.css';
import 'styles';

const store = configureStore();
const DynamicHome = lazy(() => import('./screens/Home/Home'));
window.store = store;

const Root = hot(module)(() => (
  <Provider store={store}>
    <Suspense fallback={<div>Loading...</div>}>
      <DynamicHome />
    </Suspense>
  </Provider>
));

const App = always(<Root />);

render(<App />, document.getElementById('root'));
