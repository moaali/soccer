import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'antd';

const CustomButton = props => {
  const { ...restProps } = props;

  return <Button type="primary" {...restProps} />;
};

Object.assign(CustomButton, {
  displayName: 'CustomButton',
});

export default CustomButton;
