import React, { Component } from 'react';
import PropTypes from 'prop-types';

const Video = props => {
  const { src, type, ...restProps } = props;

  return (
    <video {...restProps} width="500px">
      <source src={src} type={type} />
    </video>
  );
};

Object.assign(Video, {
  displayName: 'Video',
  propTypes: {
    src: PropTypes.string.isRequired,
    type: PropTypes.string,
  },
  defaultProps: {
    type: 'video/mp4',
  },
});

export default Video;
