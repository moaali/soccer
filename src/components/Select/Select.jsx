import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Select } from 'antd';
import { map, compose, assoc } from 'ramda';
import { PASS } from 'config';

const { Option } = Select;

const Video = props => {
  const { options, ...restProps } = props;

  return (
    <Select
      placeholder="Select an Option..."
      defaultActiveFirstOption
      allowClear
      style={{ width: '100%' }}
      {...restProps}
    >
      {options.map((value, index) => (
        <Option key={index} value={value}>
          {value}
        </Option>
      ))}
    </Select>
  );
};

Object.assign(Video, {
  displayName: 'Select',
  propTypes: {
    options: PropTypes.arrayOf(PropTypes.string),
  },
  defaultProps: {
    options: [],
  },
});

export default Video;
