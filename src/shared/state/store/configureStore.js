import { applyMiddleware, createStore, combineReducers } from 'redux';
import { createEpicMiddleware, combineEpics } from 'redux-observable';
import { BehaviorSubject } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

const epic$ = new BehaviorSubject(combineEpics());
const rootEpic = (action$, state$) =>
  epic$.pipe(mergeMap(epic => epic(action$, state$)));

const staticReducers = {
  test: () => 'test',
};

const epicMiddleware = createEpicMiddleware();
const createReducer = asyncReducers =>
  combineReducers({
    ...staticReducers,
    ...asyncReducers,
  });

const configureStore = preloadedState => {
  const enhancers = applyMiddleware(epicMiddleware);
  const store = createStore(createReducer(), enhancers);

  epicMiddleware.run(rootEpic);

  store.injectEpic = asyncEpic => epic$.next(asyncEpic);
  store.asyncReducers = {};
  store.injectReducer = (key, asyncReducer) => {
    store.asyncReducers[key] = asyncReducer;
    store.replaceReducer(createReducer(store.asyncReducers));
  };

  if (process.env.NODE_ENV !== 'production' && module.hot) {
    module.hot.accept('./reducers', () => store.replaceReducer(rootReducer));
  }

  return store;
};

export default configureStore;
