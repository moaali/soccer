import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { isEmpty } from 'ramda';
import { ReactReduxContext } from 'react-redux';

// const withReducer = (key, reducer) => WrappedComponent => {
//   const Extended = props => {
//     const context = useContext(ReactReduxContext);

//     context.store.injectReducer(key, reducer);

//     return <WrappedComponent {...props} />;
//   };

//   return Extended;
// };

const withInjector = ({ epics, reducers }) => WrappedComponent => {
  const Extended = props => {
    const {
      store: { injectReducer, injectEpic },
    } = useContext(ReactReduxContext);

    epics && !isEmpty(epics) && epics.forEach(injectEpic);
    reducers &&
      !isEmpty(reducers) &&
      reducers.forEach(([key, reducer]) => injectReducer(key, reducer));

    return <WrappedComponent {...props} />;
  };

  return Extended;
};

export default withInjector;
