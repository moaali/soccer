export const SHOT = {
  value: 'shot',
  shortcuts: {
    s: {},
  },
  fields: [
    {
      type: 'single',
      name: 'type',
      options: [
        {
          value: 'open-play',
        },
        {
          value: 'free-kick',
        },
        {
          value: 'penalty',
        },
        {
          value: 'corner',
        },
      ],
    },
    {
      type: 'single',
      name: 'body-part',
      options: [
        {
          value: 'right-foot',
          shortcut: '2',
        },
        {
          value: 'left-foot',
          shortcut: '3',
        },
        {
          value: 'head',
          shortcut: '4',
          include: {
            type: ['open-play'],
          },
        },
        {
          value: 'other',
          shortcut: '1',
          include: {
            type: ['open-play'],
          },
        },
      ],
    },
    {
      type: 'multiple',
      name: 'extras',
      options: [
        {
          value: 'aerial-won',
          include: {
            'body-part': ['head'],
          },
        },
        {
          value: 'deflected',
        },
        {
          value: 'first-time',
        },
        {
          value: 'redirect',
        },
      ],
    },
  ],
};
