import React, { memo } from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import { map, prop, pick } from 'ramda';

import { Select, Button } from 'components';
import { PASS } from 'config';

const StyledBox = styled.div`
  border-radius: 10px;
  flex-grow: 1;
  padding: 5%;
  border: 1px solid hsla(255, 100%, 96%, 1);
  margin-bottom: 5%;
  background: hsla(255, 100%, 98%, 1);
`;

const Pass = props => {
  const { pass, onChange, onSubmit, ...restProps } = props;

  const handleChange = fieldName => value => {
    onChange(fieldName, value);
  };

  return (
    <StyledBox {...restProps}>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
        }}
      >
        <h4>PASS</h4>
        <span>
          <span>E: low pass</span> <span>S: ground, open play pass</span>
        </span>
      </div>
      {PASS.fields.map((field, index) => {
        return (
          <div style={{ marginBottom: '10px' }}>
            <Select
              key={index}
              value={pass[field.name]}
              onChange={handleChange(field.name)}
              options={map(prop('value'), field.options)}
            />
          </div>
        );
      })}
      <Button onClick={onSubmit}>Submit</Button>
    </StyledBox>
  );
};

export default memo(Pass);
