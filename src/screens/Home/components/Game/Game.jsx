import React, { memo } from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';

import game from 'seeds/game.mp4';
import { Video } from 'components';

const StyledBox = styled.div`
  width: 60%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  background: hsla(255, 100%, 98%, 1);
`;

const Game = props => {
  const { ...restProps } = props;

  return (
    <StyledBox {...restProps}>
      <Video src={game} controls />
    </StyledBox>
  );
};

export default memo(Game);
