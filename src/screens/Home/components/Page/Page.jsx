import React, { memo } from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';

const StyledBox = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  height: 100vh;
`;

const Page = props => {
  const { ...restProps } = props;

  return <StyledBox {...restProps} />;
};

export default memo(Page);
