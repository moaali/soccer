import React, { memo } from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';

const StyledBox = styled.div`
  width: 40%;
  display: flex;
  flex-direction: column;
  grid-area: item2;
  background: hsla(255, 100%, 100%, 1);
  justify-self: stretch;
  padding: 30px;
`;

const Widgets = props => {
  const { ...restProps } = props;

  return <StyledBox {...restProps} />;
};

export default memo(Widgets);
