import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { pick } from 'ramda';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import { GlobalHotKeys } from 'react-hotkeys';

import { withInjector } from 'utils';
import { formReducer, select, submit, reset, submitEpic } from './ducks';
import { Game, Page, Pass, Shot, Widgets } from './components';

const keyMap = {
  lowPass: 'e',
  lowPassOpenPlay: 'w',
  shot: 's',
};

const Home = memo(props => {
  const { select, reset, submit, pass, shot, ...restProps } = props;

  const handleChange = form => (fieldName, value) => {
    select([form, fieldName, value]);
  };

  const handlers = {
    lowPass: event => select(['pass', 'height', 'low']),
    lowPassOpenPlay: event => {
      select(['pass', 'height', 'ground']);
      select(['pass', 'type', 'open-play']);
    },
    shot: event => select(['shot', 'type', 'open-play']),
  };

  const handleSubmit = form => () =>
    submit({
      form,
    });

  return (
    <GlobalHotKeys keyMap={keyMap} handlers={handlers}>
      <Page>
        <Game />
        <Widgets>
          <Pass
            onSubmit={handleSubmit('pass')}
            pass={pass}
            onChange={handleChange('pass')}
          />
          <Shot
            onSubmit={handleSubmit('shot')}
            shot={shot}
            onChange={handleChange('shot')}
          />
        </Widgets>
        <ToastContainer />
      </Page>
    </GlobalHotKeys>
  );
});

Object.assign(Home, {
  displayName: 'Home',
});

const mapStateToProps = state => pick(['pass', 'shot'], state.form);

const mapDispatchToProps = dispatch =>
  bindActionCreators({ select, reset, submit }, dispatch);

export default compose(
  withInjector({
    epics: [submitEpic],
    reducers: [['form', formReducer]],
  }),
  connect(mapStateToProps, mapDispatchToProps)
)(Home);
