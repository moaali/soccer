import { ofType } from 'redux-observable';
import { of, pipe, throwError } from 'rxjs';
import { switchMap, pluck, map, tap, mapTo, catchError } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { toast } from 'react-toastify';
import { struct } from 'superstruct';

import { BASE_URL } from 'constants';
import { submit, reset, submitSuccess, submitError } from './actions';

const Pass = struct({
  type: 'string',
  height: 'string',
  'body-part': 'string',
  extras: 'string',
});

const Shot = struct({
  type: 'string',
  'body-part': 'string',
  extras: 'string',
});

const validate = (key, form) =>
  ({
    pass: () => Pass(form),
    shot: () => Shot(form),
  }[key]());

export const submitEpic = (action$, state$) =>
  action$.pipe(
    ofType(submit.toString()),
    switchMap(action =>
      of(action).pipe(
        tap(({ payload: { form } }) => console.log('xxxx', form)),
        map(action => {
          const {
            payload: { form },
          } = action;

          validate(form, state$.value.form[form]);

          return action;
        }),
        catchError(error => {
          toast.error('Invalid Data!');
        })
      )
    ),
    switchMap(action => {
      const {
        payload: { form },
      } = action;

      return ajax({
        url: `${BASE_URL}/${form}`,
        method: 'POST',
        body: state$.value.form[form],
      }).pipe(
        map(() => {
          toast.success('Success!');
        }),
        mapTo({ type: reset.toString(), payload: form }),
        catchError(error => {
          toast.error('Error!');
        })
      );
    })
  );
