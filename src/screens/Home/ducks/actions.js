import { createAction } from '@reduxjs/toolkit';

export const select = createAction('SELECT');
export const submit = createAction('SUBMIT');
export const submitSuccess = createAction('SUBMIT_SUCCESS');
export const submitError = createAction('SUBMIT_FAILURE');
export const reset = createAction('RESET');
