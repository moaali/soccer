import { createReducer } from '@reduxjs/toolkit';
import { assocPath } from 'ramda';

import { select, reset } from './actions';

const initialState = {
  pass: {
    type: null,
    height: null,
    'body-part': null,
    extras: null,
  },
  shot: {
    type: null,
    'body-part': null,
    extras: null,
  },
};

export const formReducer = createReducer(initialState, {
  [select]: (state, action) => {
    const { payload } = action;
    const [form, key, value] = payload;

    return assocPath([form, key], value, state);
  },

  [reset]: (state, action) => {
    const { payload } = action;

    return assocPath([payload], { ...initialState[payload] }, state);
  },
});
